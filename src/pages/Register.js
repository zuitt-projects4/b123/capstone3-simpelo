import React, {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'
import {Redirect, useHistory} from 'react-router-dom'

export default function Register(){

	const {user, setUser} = useContext(UserContext)

	const history = useHistory()

	const [firstName, setFirstName] = useState("")
	const [lastName, setLastName] = useState("")
	const [email, setEmail] = useState("")
	const [mobileNo, setMobileNo] = useState("")
	const [password, setPassword] = useState("")
	const [confirmPassword, setConfirmPassword] = useState("")

	const [isActive, setIsActive] = useState(false)

	useEffect(() => {

		if((firstName !== "" && lastName !== "" && email  !== "" && mobileNo !== "" && password !== "" && confirmPassword !== "") && (password === confirmPassword) && (mobileNo.length === 11)){
			setIsActive(true)
		}else{
			setIsActive(false)
		}

	},[firstName, lastName, email, mobileNo, password, confirmPassword])

	function registerUser(e){

		e.preventDefault()//prevents  submit event's default behavior

		fetch('https://secret-bastion-87897.herokuapp.com/users/register', {

			method: 'POST',
			headers:{
				"Content-Type":"application/json"
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password
			})

		})
		.then(res => res.json())
		.then(data => {

			console.log(data)
			// email will only be not undefined if we registered properly.
			if(data.email){

				Swal.fire({

					icon:"success",
					title:"Registration Successful!",
					text: `Thank you for registering. ${data.email}`

				})

				//redirect our user after registering to our login page
				history.push('/login')

			}else{

				Swal.fire({

					icon:"error",
					title:"Registration Failed.",
					text: data.message

				})

			}
		})

	}


	return(

		user.id 
		?
		<Redirect to="/products" />
		:
		<>
			<h1 className="my-5 text-center" id="register-title">Register</h1>
			<Form onSubmit={e => registerUser(e)}>
				<Form.Group>
					<Form.Label className="form-label">First Name:</Form.Label>
					<Form.Control type="text" value={firstName} onChange={e=>{setFirstName(e.target.value)}} placeholder="Enter First Name" required/> 
				</Form.Group>
				<Form.Group>
					<Form.Label className="form-label">Last Name:</Form.Label>
					<Form.Control type="text" value={lastName} onChange={e=>{setLastName(e.target.value)}} placeholder="Enter Last Name" required/> 
				</Form.Group>
				<Form.Group>
					<Form.Label className="form-label">Email:</Form.Label>
					<Form.Control type="email" value={email} onChange={e=>{setEmail(e.target.value)}} placeholder="Enter email" required/> 
				</Form.Group>
				<Form.Group>
					<Form.Label className="form-label">Mobile No:</Form.Label>
					<Form.Control type="number" value={mobileNo} onChange={e=>{setMobileNo(e.target.value)}} placeholder="Enter 11 Digit Mobile No." required/> 
				</Form.Group>
				<Form.Group>
					<Form.Label className="form-label">Password:</Form.Label>
					<Form.Control type="password" value={password} onChange={e=>{setPassword(e.target.value)}} placeholder="Enter Password" required/> 
				</Form.Group>
				<Form.Group>
					<Form.Label className="form-label">Confirm Password:</Form.Label>
					<Form.Control type="password" value={confirmPassword} onChange={e=>{setConfirmPassword(e.target.value)}} placeholder="Confirm Password" required/> 
				</Form.Group>
				{
					isActive
					?<Button variant="primary" type="submit" className="my-3">Submit</Button>
					:<Button variant="primary" className="my-3" disabled >Submit</Button>
				}
			</Form>
		</>

	)
}