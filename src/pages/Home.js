//imports
import React, {useContext} from 'react';
import UserContext from '../UserContext'
import Banner  from '../components/Banner'

export default function Home() {


	const {user} = useContext(UserContext)


	let userContent ={
		
		title: "Louise Shop",
		description: "A Shop For Baking Needs",
		buttonCallToAction: "Shop Now", 
		destination:"/login"
	}

	let adminContent ={
		
		title: `Welcome Admin`,
		description: "",
		buttonCallToAction: "Dashboard", 
		destination:"/products"
	}

	return(

		user.isAdmin
		?
		<Banner bannerProp={adminContent}/>	
		:
		<Banner bannerProp={userContent}/>	
		
	)

}