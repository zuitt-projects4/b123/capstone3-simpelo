import React, {useEffect, useContext} from 'react'
import Banner from '../components/Banner'
import UserContext from '../UserContext'

export default function Logout(){

	const {setUser, unsetUser} = useContext(UserContext)

	unsetUser()
	useEffect(() => {

		setUser({

			id:null,
			isAdmin:null

		})

	}, [])

	const bannerContent ={

		title: "See You Later!",
		description: "You have logged out of Louise Shop",
		buttonCallToAction: "Go Back To Home Page",
		destination: "/"


	}
	return(
		
		<Banner bannerProp={bannerContent}/>

	)

}